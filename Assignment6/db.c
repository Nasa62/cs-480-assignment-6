// Assignment 6
// Repository: https://git.dulake.net/cs-480-assignment-6
// - A multithreaded flat-file database thingy.
//
// File: db.c
// Author: Chad Dulake
// - Database main file, initializes and runs threads which use mutex locking and write 10 people to the database each then remove 9 each.
//
// #######################
// # COMPILE INFORMATION #
// ####################### 
// I compiled using these exact settings with GCC 3.4.4 in Cygwin:
//  gcc -std=c99 -o db.exe db.c
//

// Uncommenting this next line will cause the program to print out every addition & removal of a person to/from the database.
//#define VERBOSE 

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <strings.h>
#include <pthread.h>

#define person_nameSize 28

typedef struct Person Person;

struct Person
{
	unsigned int id;
	char name[person_nameSize];
};

const char databaseFilePath[] = "./dulake.bin";
const unsigned char threadCount = 5;
const Person nullPerson = {0};

int databaseFileDescriptor;
pthread_mutex_t lock;

void* threadMain(void* arg);
Person* readLine(int line);
void writeLine(int line, Person* person);
void add(Person* person);
void removePerson(char* name);
int get(char* name);
void printPerson(Person* person);
void printDB();

/* main()
 * Opens the database file and creates several threads that execute threadMain, when all threads have completed prints out the database.
 */
int main(int argc, char* argv[])
{
	pthread_t threads[threadCount];
	int customThreadID[threadCount];

	// Cleanup from previous runs
	remove(databaseFilePath);

	if((databaseFileDescriptor = open(databaseFilePath, O_RDWR | O_BINARY | O_CREAT, 0644)) < 0)
	{
		fprintf(stderr, "Error opening/creating database file for reading & writing\n");
		return 1;
	}

	if(pthread_mutex_init(&lock, NULL) != 0)
	{
		fprintf(stderr, "Mutex init falure!\n");
		return 1;
	}

	for(int i = 0; i < threadCount; i++)
	{
		customThreadID[i] = i;
		if(pthread_create(&threads[i], NULL, &threadMain, &customThreadID[i]) != 0)
			fprintf(stderr, "Some sort of pThread error occured!\n");
	}

	// Wait on threads to terminate.
	for(int i = 0; i < threadCount; i++)
		pthread_join(threads[i], NULL);

	pthread_mutex_destroy(&lock);

	printDB();

	close(databaseFileDescriptor);
	return 0;
}

/* threadMain()
 * Adds 10 Persons to the database then removes 9 Persons from the database.
 */
void* threadMain(void* arg)
{
	int customThreadID = *(int*) arg;

	/* Critical Section */
	pthread_mutex_lock(&lock);

	Person* newPerson = calloc(1, sizeof(Person));
	newPerson->id = (unsigned int) customThreadID;
	sprintf(newPerson->name, "Thread_%u", newPerson->id);
	for(int i = 0; i < 10; i++)
		add(newPerson);
	for(int i = 0; i < 9; i++)
		removePerson(newPerson->name);
	free(newPerson);

	pthread_mutex_unlock(&lock);
	/* End critical section */
}

/* readLine()
 * reads the specified line from the file
 * returns NULL if past end of file or out of bounds.
 * returns the Person read from specified line
 * Assumes the database being read was written by the same version of this program (has the same sizes)
 */
Person* readLine(int line)
{
	Person* result = (Person*) malloc(sizeof(Person));
	int readBytes;
	if(lseek(databaseFileDescriptor, line * sizeof(Person), SEEK_SET) < 0)
		return NULL; // EOF/Out of bounds
	readBytes = read(databaseFileDescriptor, result, sizeof(Person));
	if(readBytes == 0)
		return NULL; // EOF
	if(readBytes != sizeof(Person))
		fprintf(stderr, "Warning # of bytes read does not match the size of a Person\n");

	return result;
}

/* writeLine()
 * writes the specified line to the file
 * Line numbers start at 0
 * Line number of -1 means append, -2 means overwrite last line.
 * Line number of 0 means write first line, 1 means write second line.
 */
void writeLine(int line, Person* person)
{
	int seekMode = line >= 0 ? SEEK_SET : SEEK_END;
	if(line < 0)
		line++;
	if(lseek(databaseFileDescriptor, line * sizeof(Person), seekMode) < 0)
		return; // Error

	if(write(databaseFileDescriptor, person, sizeof(Person)) < 0)
		return; // Error
}

/* add(Person)
 * Inserts a person into the database
 * The person's name & number should be printed to stdout letting the user know they have been added successfully.
 */
void add(Person* person)
{
	writeLine(-1, person); // Append the new person to the end of the file
#ifdef VERBOSE
	fprintf(stdout, "ADD    - ");
	printPerson(person);
#endif
}

/* removePerson(char* name)
 * name must be a C String.
 * name is case-insensitive.
 * Removes the person from the database & compacts the gap where the record was.
 * The person's name & id number is printed to stdout if removal was successful.
 */
void removePerson(char* name)
{
	int readBytes;
	Person* personBuffer;
	for(int i = 0; 1; i++)
	{
		personBuffer = readLine(i);
		if(personBuffer == NULL)
			break; // EOF
		if(strcasecmp(personBuffer->name, name) != 0)
		{
			free(personBuffer);
			continue;
		}
#ifdef VERBOSE
		fprintf(stdout, "REMOVE - ");
		printPerson(personBuffer);
#endif
		free(personBuffer);
		for(int j = 0; 1; j++)
		{
			personBuffer = readLine(i + j + 1);
			if(personBuffer == NULL)
			{
				int fSize = lseek(databaseFileDescriptor, -sizeof(Person), SEEK_END);
				ftruncate(databaseFileDescriptor, fSize);
				return; // EOF
			}
			writeLine(i + j, personBuffer);
			free(personBuffer);
		}
	}
}

/* get(char* name)
 * Retrieves the person's id number.
 * Main function should print the number returned to STDOUT.
 * Returns -1 if no result.
 */
int get(char* name)
{
	Person* personBuffer;
	int readBytes;
	for(int i = 0; 1; i++)
	{
		personBuffer = readLine(i);
		if(personBuffer == NULL)
			break;

		if(strcasecmp(personBuffer->name, name) == 0)
		{
			int result = personBuffer->id;
			free(personBuffer);
			return result;
		}

		free(personBuffer);
	}
	return -1;
}

/* printPerson()
 * Prints the person's information to stdout
 * Assures proper handling if the name is not a null terminated string
 */
void printPerson(Person* person)
{
	fprintf(stdout, "ID: %u, NAME: ", person->id);
	fwrite(person->name, sizeof(char), person_nameSize, stdout);
	fprintf(stdout, "\n");
}

/* printDB()
 * prints the name & id numbers of everyone in the database
 */
void printDB()
{
	Person* personBuffer;
	for(int i = 0; 1; i++)
	{
		personBuffer = readLine(i);
		if(personBuffer == NULL)
			break;

		printPerson(personBuffer);
		free(personBuffer);
	}
}
