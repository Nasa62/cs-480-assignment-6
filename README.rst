By: Chad Dulake

========================================
CS 480 - Operating Systems - Spring 2013
========================================

Assignment 6
============
A multi-threaded flat file database

How to compile & run
--------------------
Tested only with GCC 3.4.4 under Cygwin on Windows 7 64 bit

**To compile use:**
    ``gcc -std=c99 -o db.exe db.c``
        If you wish to have the program output all additions & removals of 'persons' from the database, uncomment the line ``#define VERBOSE`` from near the top of ``db.c`` and compile in the same way.

**To execute:**
    Simply run the executable with no arguments.

